# syntax=docker/dockerfile:1

FROM python:3.9-slim
RUN apt update && apt dist-upgrade -y
RUN apt install git -y
RUN pip3 install git+https://gitlab.freedesktop.org/dbaker/mr-label-maker.git

CMD [ "mr_label_maker", "--project", "mesa", "--poll", "600", "--merge-requests", "--issues" ]
